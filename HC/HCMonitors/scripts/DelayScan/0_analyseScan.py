from ROOT import gSystem
gSystem.Load('HRCTuple_C')
from ROOT import * 
from array import *
import sys
from math import *

# Configuration
from optparse import OptionParser
parser = OptionParser()
parser.add_option("-r", "--runNumber", type="string",
                  help="Run number to be analysed",
                  dest="runNumber", default = "-1")

parser.add_option("--bxid_headOfTrain_bb", type="string",
                  help="bxID used to trigger central crossing at the head of a bb train (i.e. bxid-1 is the bxid of the nearest bb)",
                  dest="bxid_headOfTrain_bb", default = -1)

parser.add_option("--bxid_tailOfTrain_bb", type="string",
                  help="bxID used to trigger central crossing at the tail of a bb train (i.e. bxid+1 is the bxid of the nearest bb)",
                  dest="bxid_tailOfTrain_bb", default = -1)

parser.add_option("--bxid_headOfTrain_ee", type="string",
                  help="bxID used to trigger central crossing at the head of a ee train (i.e. this should be far from a bb train and should match the parity of the bxid_headOfTrain_bb",
                  dest="bxid_headOfTrain_ee", default = -1)

parser.add_option("--bxid_tailOfTrain_ee", type="string",
                  help="bxID used to trigger central crossing at the tail of a ee train (i.e. this should be far from a bb train and should match the parity of the bxid_tailOfTrain_bb",
                  dest="bxid_tailOfTrain_ee", default = -1)

options, arguments = parser.parse_args()

## Get the dataset
'''
Can either specify several files with same Tuple name or several tuple in the same file (ie either TAE decoded from the same run or different runs for each latency)

The script considers four bxids. 
1) bb_head i.e. the bxid at the 'top' of a bunch train (i.e. the nearest bb crossing is at bxid-1)
2) bb_tail i.e. the bxid at the 'bottom' of a bunch train (i.e. the nearest bb crossing is at bxid+1)
3) ee_head i.e. parity matches bb_head but far from bb train
3) ee_tail i.e. parity matches bb_tail but far from bb train
'''

slots = ['Central','Prev','Next']#,'PrevPrev','NextNext']
'''
datasetsFiles = {'Central':'/home/herschel/AnalysedRuns/172097/NTuple_172097Tuple.root',
                 'Prev':'/home/herschel/AnalysedRuns/172122/NTuple_172122Tuple.root',
                 'Next':'/home/herschel/AnalysedRuns/172131/NTuple_172131Tuple.root',
                 'Reference':'/home/herschel/AnalysedRuns/172187/NTuple_172187Tuple.root'
            }
datasetsTuple = {'Central':'HCDigitTuple',
            'Prev':'HCDigitTuple',
            'Next':'HCDigitTuple',
            'Reference':'HCDigitTuple'
            }
'''
datasetsFiles = {'Central':'/home/herschel/AnalysedRuns/'+options.runNumber+'/NTuple_'+options.runNumber+'Tuple.root',
                 'Prev':'/home/herschel/AnalysedRuns/'+options.runNumber+'/NTuple_'+options.runNumber+'Tuple.root',
                 'Next':'/home/herschel/AnalysedRuns/'+options.runNumber+'/NTuple_'+options.runNumber+'Tuple.root',
                 'PrevPrev':'/home/herschel/AnalysedRuns/'+options.runNumber+'/NTuple_'+options.runNumber+'Tuple.root',
                 'NextNext':'/home/herschel/AnalysedRuns/'+options.runNumber+'/NTuple_'+options.runNumber+'Tuple.root'
            }
datasetsTuple = {'Central':'HCDigitTuple',
            'Prev':'HCDigitTuplePrev1',
            'Next':'HCDigitTupleNext1',
            'PrevPrev':'HCDigitTuplePrev2',
            'NextNext':'HCDigitTupleNext2',
            'Reference':'HCDigitTuple'
            }

import random

def randomize(adc) :
    if adc<128.5:return adc
    elif adc<255.5:return adc+int(random.uniform(0,2))
    elif adc<511.5:return adc+int(random.uniform(0,8))
    else: return adc+int(random.uniform(0,16))

maxStep = 1024

datasets = {}
for slot in slots:
    print datasetsFiles[slot]
    print datasetsTuple[slot]
    datasets[slot] = HRCTuple(datasetsFiles[slot],datasetsTuple[slot],None)

print 'got here'
### Create outputFile
f = TFile('Scans/Scan_'+options.runNumber+'.root','RECREATE')

### Create the historgrams for scan analysis
hist = {}
for bxid in ['bb_head','bb_tail','ee_head','ee_tail'] :
    hist[bxid] = {}
    for slot in slots:   
        hist[bxid][slot] = {}
        for station in ['B0','B1','B2','F1','F2']:
            hist[bxid][slot][station] = {}
            for q in ['0','1','2','3']:
                hist[bxid][slot][station][q]={}
                hist[bxid][slot][station][q]['2D']=TH2D(slot+station+q+'_'+str(bxid),slot+station+q+'_'+bxid,maxStep,0.,maxStep,1024,0.,1024.)
                for step in xrange(maxStep):
                    hist[bxid][slot][station][q][step]= TH1D(slot+str(step)+station+q+'_'+str(bxid),slot+str(step)+station+q+'_'+bxid,1024,0.,1024.)

### Fill the histograms
for slot in slots: 
    print 'Treating',slot,datasets[slot].fChain.GetEntries()
    for n in xrange(datasets[slot].fChain.GetEntries()): 
        if n%1000 ==0:
            i = round(float(n)/datasets[slot].fChain.GetEntries()*100,0)
            sys.stdout.write("\r    |        Looping on the ntuple: Progress %d%%" %i) 
            sys.stdout.flush()
        datasets[slot].GetEntry(n)  
        if datasets[slot].step>maxStep-1: continue
        ## what is the central bxID parity
        bxidParity = 'Odd'
        if slot == 'Central' and datasets[slot].bxID%2 == 0:bxidParity = 'Even'
        if ( slot == 'Prev' or slot == 'Next' ) and datasets[slot].bxID%2 == 1: bxidParity = 'Even'
        if ( slot == 'PrevPrev' or slot == 'NextNext' ) and datasets[slot].bxID%2 == 0: bxidParity = 'Even'
        #if not datasets[slot].bxID() in bxIDList: continue
        for station in ['B0','B1','B2','F1','F2']:
            for q in ['0','1','2','3']:
                adc = randomize(getattr(datasets[slot],station+q))
                hist[bxidParity][slot][station][q]['2D'].Fill(int(datasets[slot].step)+0.5,adc)
                hist[bxidParity][slot][station][q][int(datasets[slot].step)].Fill(adc)  
    print ''

for bxid in  ['Odd','Even']:
    for station in ['B0','B1','B2','F1','F2']:
        for q in ['0','1','2','3']:
            for slot in slots:   
                hist[bxid][slot][station][q]['2D'].Write()  
                for step in xrange(maxStep):
                    hist[bxid][slot][station][q][step].Write()  

f.Close()
