#pragma once

// Gaudi
#include "GaudiAlg/GaudiTupleAlg.h"

// Local
#include "HCMonitorBase.h"

class Condition;

/** @class HCDigitCorrector.h
 *
 * Apply common mode suppression to raw ADC values.
 *
 */

class HCDigitCorrector final : public HCMonitorBase {
 public:
  /// Standard constructor
  HCDigitCorrector(const std::string& name, ISvcLocator* pSvcLocator);

  virtual StatusCode initialize() override;  ///< Algorithm initialization
  virtual StatusCode execute()    override;  ///< Algorithm execution

 private:
  std::string m_inputLocation;
  std::string m_outputLocation;

  Condition* m_condCM = nullptr;

  /// Calibration constants for each channel and parity
  std::vector<std::vector<std::vector<float> > > m_tantheta;
  std::vector<std::vector<std::vector<float> > > m_x0;
  std::vector<std::vector<std::vector<float> > > m_y0;

  // Input parameters  
  std::vector<float> m_thetaConfig;
  std::vector<float> m_x0Config;
  std::vector<float> m_y0Config;

  StatusCode cacheParameters();
  StatusCode setParameters();
 
};
