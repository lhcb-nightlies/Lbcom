#ifndef HCDELAYSCAN_H
#define HCDELAYSCAN_H 1

// AIDA
#include "AIDA/IHistogram1D.h"
#include "AIDA/IHistogram2D.h"
#include "AIDA/IProfile1D.h"

// ROOT
#include "TH2D.h"
#include "TProfile.h"

// Local
#include "HCMonitorBase.h"

/** @class HCDigitMonitor HCDigitMonitor.h
 *
 *  Monitoring algorithm for Herschel digits.
 *
 */

class HCDelayScan final : public HCMonitorBase {
 public:

  using HCMonitorBase::HCMonitorBase;

  virtual StatusCode initialize() override;  ///< Algorithm initialization
  virtual StatusCode execute()    override;  ///< Algorithm execution
  
  /// TES location of digits.
  Gaudi::Property<std::string> m_digitLocation{this, "DigitLocation", LHCb::HCDigitLocation::Default,      "TES location of Herschel digits"};
  Gaudi::Property<std::string> m_digitLocationPrev{this, "DigitLocationPrev", "Prev1/" + LHCb::HCDigitLocation::Default,      "TES location of Prev Herschel digits"};
  Gaudi::Property<std::string> m_digitLocationNext{this, "DigitLocationNext", "Next1/" + LHCb::HCDigitLocation::Default,      "TES location of Next Herschel digits"};

  Gaudi::Property<unsigned int> m_minStep{this, "MinimumStepNr", 0,      "Delay scan minimum step"};
  Gaudi::Property<unsigned int> m_maxStep{this, "MaximumStepNr", 0,      "Delay scan maximum step"};
  
  /// BXID labels
  Gaudi::Property<unsigned int> m_bxid_bb_head{this, "BXIDbbHead", 0,      "BXID for bb head"};
  Gaudi::Property<unsigned int> m_bxid_bb_tail{this, "BXIDbbTail", 0,      "BXID for bb tail"};
  Gaudi::Property<unsigned int> m_bxid_ee_head{this, "BXIDeeHead", 0,      "BXID for ee head"};
  Gaudi::Property<unsigned int> m_bxid_ee_tail{this, "BXIDeeTail", 0,      "BXID for ee tail"};
 
 private:

  /// Profile histograms (ADC vs. step) for each channel, slot and parity.
  std::vector<std::vector<std::vector<std::vector<TProfile*> > > > m_adcs;
  /// ADC distributions for each channel, slot, parity and step.
  std::vector<std::vector<std::vector<std::vector<std::vector<TH1D* > > > > > m_adcsPerStep;
};

#endif
