#include <mutex>

// ROOT
#include "TH2D.h"
#include "TAxis.h"

// Gaudi
#include "GaudiUtils/Aida2ROOT.h"
#include "GaudiUtils/HistoLabels.h"

// LHCb
#include "DetDesc/Condition.h"
// Event/DAQEvent
#include "Event/ODIN.h"
// Event/DigiEvent
#include "Event/HCDigit.h"
// Kernel/LHCbKernel
#include "Kernel/HCCellID.h"
// Kernel/HltInterfaces
#include "Kernel/ReadRoutingBits.h"

// Local
#include "HCDigitMonitor.h"

#ifdef ONLINEMONITORING
// Online
#include "dim/dic.hxx"
#endif

using namespace Gaudi::Utils::Histos;

DECLARE_ALGORITHM_FACTORY(HCDigitMonitor)

#ifdef ONLINEMONITORING
namespace  {
  bool s_schema_changed = false;
  std::mutex s_dim_mutex;
  void schema_feed(void* tag, void* address, int* size) {
    if (address && tag && size && *size > 0) {
      std::string* val = *(std::string**)tag;
      std::lock_guard<std::mutex> guard(s_dim_mutex);
      *val = (char*)address;
      s_schema_changed = true;
    }
  }
}
#endif

//=============================================================================
// Standard constructor
//=============================================================================
HCDigitMonitor::HCDigitMonitor(const std::string& name,
                               ISvcLocator* pSvcLocator)
: HCMonitorBase(name, pSvcLocator)   {
  declareProperty("DigitLocation",
                  m_digitLocation = LHCb::HCDigitLocation::Default);
  declareProperty("ADCvsBX", m_adcVsBx = false);
  declareProperty("AllChannels", m_allChannels = false); 
}

//=============================================================================
// Initialisation
//=============================================================================
StatusCode HCDigitMonitor::initialize() {

  // Initialise the base class.
  StatusCode sc = HCMonitorBase::initialize();
  if (sc.isFailure()) return sc;

  // Setup the histograms.
  m_hBxTypes = book1D("BxTypes", "Bx", -0.5, 3.5, 4);
  m_hTriggerTypes = book1D("TriggerTypes", "Trigger", -0.5, 7.5, 8);
  m_hRoutingBits = book1D("RoutingBits", "Routing Bits", -0.5, 127.5, 128);
  m_hMissingQuads = book1D("MissingQuads", "Missing Quadrants", -0.5, 19.5, 20);
  m_hFillingScheme = book2D("FillingScheme", "Filling Scheme", 
                            -0.5, 3564.5, 3565, -0.5, 4.5, 5); 
  TH2D* h = Gaudi::Utils::Aida2ROOT::aida2root(m_hFillingScheme);
  if (h) {
    TAxis* axis = h->GetYaxis();
    if (axis) {
      axis->SetBinLabel(1, "B1");
      axis->SetBinLabel(2, "B2");
      axis->SetBinLabel(3, "Spill-over");
      axis->SetBinLabel(4, "Pre-spill");
      axis->SetBinLabel(5, "Empty");
    }
  } 
  const std::vector<std::string> stations = {"B0", "B1", "B2", "F1", "F2"};
  const unsigned int nStations = 5;
  m_hAdcSum.resize(nStations);
  m_hAdcSumNoBeam.resize(nStations);
  m_hAdcSumBeam.resize(nStations);
  m_hAdcVsQuadrant.resize(nStations);
  m_hAdcVsQuadrantNoBeam.resize(nStations);
  m_hAdcVsQuadrantBeam.resize(nStations);
  for (unsigned int i = 0; i < nStations; ++i) {
    const unsigned int bins = m_variableBins ? 256 : 4 * m_parADC.bins();
    const double low = m_parADC.lowEdge();
    const double high = 4 * (m_parADC.highEdge() + 0.5) - 0.5;
    const std::string st = stations[i];
    const std::vector<std::string> bcs = {"/Even/", "/Odd/", "/"};
    for (const auto& bc : bcs) {
      // Book histograms for ADC sum distributions.
      std::string name = "ADC/Sum" + bc + st;
      m_hAdcSum[i].push_back(book1D(name, st, low, high, bins));
      name = "ADC/Sum/NoBeam" + bc + st;
      m_hAdcSumNoBeam[i].push_back(book1D(name, st, low, high, bins));
      name = "ADC/Sum/Beam" + bc + st;
      m_hAdcSumBeam[i].push_back(book1D(name, st, low, high, bins));
      setAxisLabels(m_hAdcSum[i].back(), "Sum ADC", "Entries");
      setAxisLabels(m_hAdcSumNoBeam[i].back(), "Sum ADC", "Entries");
      setAxisLabels(m_hAdcSumBeam[i].back(), "Sum ADC", "Entries");
      // Book profile histograms of average ADC vs. quadrant.
      name = "ADC/" + st + bc + "Average";
      m_hAdcVsQuadrant[i].push_back(bookProfile1D(name, st, -0.5, 3.5, 4));
      name = "ADC/" + st + "/NoBeam" + bc + "Average";
      m_hAdcVsQuadrantNoBeam[i].push_back(bookProfile1D(name, st, -0.5, 3.5, 4));
      name = "ADC/" + st + "/Beam" + bc + "Average";
      m_hAdcVsQuadrantBeam[i].push_back(bookProfile1D(name, st, -0.5, 3.5, 4));
      setAxisLabels(m_hAdcVsQuadrant[i].back(), "Quadrant", "ADC");
      setAxisLabels(m_hAdcVsQuadrantNoBeam[i].back(), "Quadrant", "ADC");
      setAxisLabels(m_hAdcVsQuadrantBeam[i].back(), "Quadrant", "ADC");
    }
  }

  const std::array<std::string, 2> sides = {"B", "F"};
  const unsigned int nChannels = 64;
  m_hAdcChannel.resize(2 * nChannels);
  m_hAdcVsChannel.resize(2); 
  for (unsigned int i = 0; i < 2; ++i) {
    const std::string side = sides[i];
    // Book profile histograms of average ADC vs. channel number.
    m_hAdcVsChannel[i].push_back(
      bookProfile1D("ADC/" + side + "/Even/Average", side, -0.5, 63.5, 64));
    m_hAdcVsChannel[i].push_back(
      bookProfile1D("ADC/" + side + "/Odd/Average", side, -0.5, 63.5, 64));
    m_hAdcVsChannel[i].push_back(
      bookProfile1D("ADC/" + side + "/Average", side, -0.5, 63.5, 64));
    for (auto profile : m_hAdcVsChannel[i]) {
      setAxisLabels(profile, "Channel", "ADC");
    }
    for (unsigned int j = 0; j < nChannels; ++j) {
      const unsigned int index = i * nChannels + j;
      // Book histograms for ADC distributions for each channel.
      const std::string ch = "Channel" + std::to_string(j);
      const std::vector<std::string> bcs = {"/Even/", "/Odd/", "/"};
      for (const auto& bc : bcs) {
        const std::string name = "ADC/" + side + bc + ch;
        if (m_variableBins) {
          m_hAdcChannel[index].push_back(book1D(name, ch, m_edges));
        } else {
          const double low = m_parADC.lowEdge();
          const double high = m_parADC.highEdge();
          const unsigned int bins = m_parADC.bins();
          m_hAdcChannel[index].push_back(book1D(name, ch, low, high, bins));
        }
        setAxisLabels(m_hAdcChannel[index].back(), "ADC", "Entries");
      }
    }
  }

  m_hAdcQuadrant.resize(4 * nStations);
  m_hAdcQuadrantNoBeam.resize(4 * nStations);
  m_hAdcQuadrantBeam.resize(4 * nStations);
  m_hAdcQuadrantSpillOver.resize(4 * nStations);
  m_hAdcQuadrantPreSpill.resize(4 * nStations);
  m_hAdcQuadrantEmpty.resize(4 * nStations);
  m_hCorrelation.resize(4 * nStations);
  for (unsigned int i = 0; i < 4; ++i) {
    // Histogram range and number of bins (in case of uniform binning).
    const double low = m_parADC.lowEdge();
    const double high = m_parADC.highEdge();
    const unsigned int bins = m_parADC.bins();
    // Book histograms for ADC distributions for each quadrant.
    const std::string qu = "Quadrant" + std::to_string(i);
    // Bx ID binning
    const double bxlow = -0.5;
    const double bxhigh = 4095.5;
    const unsigned int bxbins = 4096;
    std::vector<double> bxedges;
    for (unsigned int j = 0; j < bxbins + 1; ++j) {
      bxedges.push_back(j - 0.5);
    }
    for (unsigned int j = 0; j < nStations; ++j) {
      const unsigned int index = i * nStations + j;
      const std::string st = stations[j];
      const std::string nameSO = "ADC/" + st + "/SpillOver/" + qu;
      const std::string namePS = "ADC/" + st + "/PreSpill/" + qu;
      const std::string nameEmpty = "ADC/" + st + "/Empty/" + qu;
      m_hAdcQuadrantSpillOver[index] = book1D(nameSO, qu, low, high, bins);
      m_hAdcQuadrantPreSpill[index] = book1D(namePS, qu, low, high, bins);
      m_hAdcQuadrantEmpty[index] = book1D(nameEmpty, qu, low, high, bins);
      setAxisLabels(m_hAdcQuadrantSpillOver[index], "ADC", "Entries");
      setAxisLabels(m_hAdcQuadrantPreSpill[index], "ADC", "Entries");
      setAxisLabels(m_hAdcQuadrantEmpty[index], "ADC", "Entries");
      const std::string nameCorr = "Correlation/" + st + "/" + qu;
      m_hCorrelation[index] = book2D(nameCorr, qu, -0.5, 1023.5, 200, 
                                                   -0.5, 1023.5, 200); 
      setAxisLabels(m_hCorrelation[index], "Mean ADC", "ADC Quadrant");
      const std::vector<std::string> bcs = {"/Even/", "/Odd/", "/"};
      for (const auto& bc : bcs) {
        const std::string name = "ADC/" + st + bc + qu;
        const std::string nameNoBeam = "ADC/" + st + "/NoBeam" + bc + qu;
        const std::string nameBeam = "ADC/" + st + "/Beam" + bc + qu;
        if (m_variableBins) {
          m_hAdcQuadrant[index].push_back(book1D(name, qu, m_edges));
          m_hAdcQuadrantNoBeam[index].push_back(book1D(nameNoBeam, qu, m_edges));
          m_hAdcQuadrantBeam[index].push_back(book1D(nameBeam, qu, m_edges));
        } else {
          m_hAdcQuadrant[index].push_back(book1D(name, qu, low, high, bins));
          m_hAdcQuadrantNoBeam[index].push_back(book1D(nameNoBeam, qu, low, high, bins));
          m_hAdcQuadrantBeam[index].push_back(book1D(nameBeam, qu, low, high, bins));
        }
        setAxisLabels(m_hAdcQuadrant[index].back(), "ADC", "Entries");
        setAxisLabels(m_hAdcQuadrantNoBeam[index].back(), "ADC", "Entries");
        setAxisLabels(m_hAdcQuadrantBeam[index].back(), "ADC", "Entries");
      }
      if (!m_adcVsBx) continue;
      const std::string nameBx = "ADCvsBX/" + stations[j] + "/" + qu;
      if (m_variableBins) {
        m_hAdcVsBx.push_back(book2D(nameBx, qu, bxedges, m_edges));
      } else {
        m_hAdcVsBx.push_back(
          book2D(nameBx, qu, bxlow, bxhigh, bxbins, low, high, bins));
      }
      setAxisLabels(m_hAdcVsBx[index], "BX", "ADC");
    }
  }

#ifndef ONLINEMONITORING
  const std::string location = "Conditions/Online/LHCb/LHCFillingScheme";
  if (existDet<Condition>(location)) {
    registerCondition(location, m_condFilling, 
                      &HCDigitMonitor::cacheFillingScheme);
    // First update.
    sc = updMgrSvc()->update(this);
    if (sc.isFailure()) {
      Warning("Cannot update filling scheme.").ignore();
    }
  } else {
    Warning("Cannot find " + location + " in database.").ignore();
  }
#else
  m_schemaSvc[0] = ::dic_info_service("LHC/B1FillingScheme", MONITORED, 0, 0, 
                                      0, schema_feed, (long)&m_schema[0], 0, 0);
  if (0 == m_schemaSvc[0]) {
    Warning("Failed to connect to DIM service LHC/B1FillingScheme.").ignore();
  }
  m_schemaSvc[1] = ::dic_info_service("LHC/B2FillingScheme", MONITORED, 0, 0,
                                      0, schema_feed, (long)&m_schema[1], 0, 0);
  if (0 == m_schemaSvc[1]) {
    Warning("Failed to connect to dim service LHC/B2FillingScheme.").ignore();
  }
#endif
  return StatusCode::SUCCESS;
}

//=============================================================================
// Main execution
//=============================================================================
StatusCode HCDigitMonitor::execute() {

  // Get event information from ODIN.
  const LHCb::ODIN* odin = getIfExists<LHCb::ODIN>(LHCb::ODINLocation::Default);
  if (!odin) {
    return Error("Cannot retrieve ODIN", StatusCode::SUCCESS);
  }
  const unsigned int bxid = odin->bunchId();
  // Skip events with out-of-range bunch-crossing ID.
  if (bxid < m_bxMin || bxid > m_bxMax) return StatusCode::SUCCESS;
  const unsigned int evenodd = bxid % 2;
#ifdef ONLINEMONITORING
  {
    std::lock_guard<std::mutex> guard(s_dim_mutex);
    if (s_schema_changed) {
      updateBxIds(m_schema[0], m_schema[1]);
      s_schema_changed = false;
    }
  }
#endif
  // Get the bunch-crossing type.
  const auto bxtype = odin->bunchCrossingType();
  m_hBxTypes->fill(bxtype);
  m_hTriggerTypes->fill(odin->triggerType());

  // Get the routing bits that fired.
  auto rawEvent = getIfExists<LHCb::RawEvent>(LHCb::RawEventLocation::Default);
  if (rawEvent) {
    auto banks = rawEvent->banks(LHCb::RawBank::HltRoutingBits);
    if (!banks.empty()) {
      const std::vector<unsigned int> yes = Hlt::firedRoutingBits(rawEvent);
      for (unsigned int bit : yes) m_hRoutingBits->fill(bit);
    }
  }
  // Grab the digits.
  const LHCb::HCDigits* digits = getIfExists<LHCb::HCDigits>(m_digitLocation);
  if (!digits) {
    for (unsigned int i = 0; i < 20; ++i) m_hMissingQuads->fill(i);
    return Error("No digits in " + m_digitLocation);
  }

  // Loop over all digits.
  for (const LHCb::HCDigit* digit : *digits) {
    const unsigned int crate = digit->cellID().crate();
    const unsigned int channel = digit->cellID().channel();
    const double adc = fadc(digit->adc());
    if (crate != m_crateB && crate != m_crateF) {
      Warning("Unexpected crate (" + std::to_string(crate) + ")").ignore();
    } 
    const unsigned int side = crate == m_crateF ? 1 : 0;
    m_hAdcVsChannel[side][2]->fill(channel, adc);
    m_hAdcVsChannel[side][evenodd]->fill(channel, adc);
    if (!m_allChannels) continue;
    const unsigned int offset = side * 64;
    m_hAdcChannel[offset + channel].back()->fill(adc);
    m_hAdcChannel[offset + channel][evenodd]->fill(adc);
  }

  const unsigned int nStations = 5;
  std::vector<double> sum(nStations, 0.);
  for (unsigned int i = 0; i < nStations; ++i) {
    const bool bwd = i < 3;
    for (unsigned int j = 0; j < 4; ++j) { 
      // Skip masked quadrants.
      if (m_masked[i][j]) continue;
      LHCb::HCCellID id(m_channels[i][j]);
      const LHCb::HCDigit* digit = digits->object(id);
      if (!digit) {
        const std::string st = bwd ? "B" + std::to_string(i) : 
                                     "F" + std::to_string(i - 2); 
        Warning("Cannot retrieve digit for " + st + std::to_string(j)).ignore();
        m_hMissingQuads->fill(i * 4 + j);
        continue;
      }
      const double adc = fadc(digit->adc());
      const unsigned int index = j * nStations + i;
      m_hAdcVsQuadrant[i][2]->fill(j, adc);
      m_hAdcVsQuadrant[i][evenodd]->fill(j, adc);
      m_hAdcQuadrant[index][2]->fill(adc);
      m_hAdcQuadrant[index][evenodd]->fill(adc);
      if (m_adcVsBx) m_hAdcVsBx[index]->fill(bxid, adc);
      if (bxtype == LHCb::ODIN::NoBeam) {
        m_hAdcVsQuadrantNoBeam[i][2]->fill(j, adc);
        m_hAdcVsQuadrantNoBeam[i][evenodd]->fill(j, adc);
        m_hAdcQuadrantNoBeam[index][2]->fill(adc);
        m_hAdcQuadrantNoBeam[index][evenodd]->fill(adc);
      } else if (bxtype == LHCb::ODIN::BeamCrossing) {
        m_hAdcVsQuadrantBeam[i][2]->fill(j, adc);
        m_hAdcVsQuadrantBeam[i][evenodd]->fill(j, adc);
        m_hAdcQuadrantBeam[index][2]->fill(adc);
        m_hAdcQuadrantBeam[index][evenodd]->fill(adc);
      }
      if (m_isSpillOver[bxid - 1]) {
        m_hAdcQuadrantSpillOver[index]->fill(adc);
      } else if (m_isPreSpill[bxid - 1]) {
        m_hAdcQuadrantPreSpill[index]->fill(adc);
      } else if (m_isEmpty[bxid - 1]) {
        m_hAdcQuadrantEmpty[index]->fill(adc);
      }
      sum[i] += adc;
    }
    m_hAdcSum[i][2]->fill(sum[i]);
    m_hAdcSum[i][evenodd]->fill(sum[i]);
    if (bxtype == LHCb::ODIN::NoBeam) {
      m_hAdcSumNoBeam[i][2]->fill(sum[i]);
      m_hAdcSumNoBeam[i][evenodd]->fill(sum[i]);
    } else if (bxtype == LHCb::ODIN::BeamCrossing) {
      m_hAdcSumBeam[i][2]->fill(sum[i]);
      m_hAdcSumBeam[i][evenodd]->fill(sum[i]);
    }
  }
  // Another loop to fill the correlation plots.
  for (unsigned int i = 0; i < nStations; ++i) {
    const bool bwd = i < 3;
    double avg = bwd ? sum[0] + sum[1] + sum[2] : sum[3] + sum[4];
    if (bwd) {
      avg /= 12;
    } else {
      avg /= 8;
    } 
    for (unsigned int j = 0; j < 4; ++j) { 
      // Skip masked quadrants.
      if (m_masked[i][j]) continue;
      LHCb::HCCellID id(m_channels[i][j]);
      const LHCb::HCDigit* digit = digits->object(id);
      if (!digit) continue;
      const double adc = fadc(digit->adc());
      const unsigned int index = j * nStations + i;
      if (bxtype == LHCb::ODIN::BeamCrossing) {
        m_hCorrelation[index]->fill(avg, adc);
      }
    }
  }
  return StatusCode::SUCCESS;
}

//=============================================================================
// Finalisation
//=============================================================================
StatusCode HCDigitMonitor::finalize() {

#ifdef ONLINEMONITORING
  // Release DIM services
  if (m_schemaSvc[0]) {
    ::dic_release_service(m_schemaSvc[0]);
    m_schemaSvc[0] = 0;
  }
  if (m_schemaSvc[1]) {
    ::dic_release_service(m_schemaSvc[1]);
    m_schemaSvc[1] = 0;
  }
#endif

  if (!m_variableBins) return HCMonitorBase::finalize();
  for (auto& row : m_hAdcChannel) {
    for (auto histogram : row) scale(histogram);
  }
  const unsigned int nStations = 5;
  for (unsigned int i = 0; i < 4; ++i) {
    for (unsigned int j = 0; j < nStations; ++j) {
      const unsigned int index = i * nStations + j;
      for (auto histogram : m_hAdcQuadrant[index]) scale(histogram);
      for (auto histogram : m_hAdcQuadrantNoBeam[index]) scale(histogram);
      for (auto histogram : m_hAdcQuadrantBeam[index]) scale(histogram);
    }
  }
  return HCMonitorBase::finalize();
}

//=============================================================================
// Retrieve the filling scheme and update the spill-over etc. flags.
//=============================================================================
StatusCode HCDigitMonitor::cacheFillingScheme() {

  const auto keys = m_condFilling->paramNames();
  bool foundB1 = false;
  bool foundB2 = false;
  for (const auto& key : keys) {
    if (key == "B1FillingScheme") foundB1 = true;
    if (key == "B2FillingScheme") foundB2 = true;
  }
  if (!foundB1 || !foundB2) {
    Warning("Filling scheme not found.").ignore();
  } else {
    const auto b1 = m_condFilling->param<std::string>("B1FillingScheme");
    const auto b2 = m_condFilling->param<std::string>("B2FillingScheme");
    updateBxIds(b1, b2);
  }
  return StatusCode::SUCCESS;
}

bool HCDigitMonitor::updateBxIds(const std::string& b1, const std::string& b2) {

  m_isSpillOver.reset();
  m_isPreSpill.reset();
  m_isEmpty.reset();
  const unsigned int nBins = b1.size();
  if (nBins != b2.size()) {
    Error("Filling schemes for B1 and B2 have different length.").ignore();
    return false;
  }
  if (nBins != 3564) {
    Error("Unexpected filling scheme length (" + 
          std::to_string(nBins) + ")").ignore();
    return false;
  }
  unsigned int nColliding = 0;
  for (unsigned int i = 2; i < nBins - 2; ++i) {
    if (b1[i] == '1' && b2[i] == '1') ++nColliding;
    if (b1[i] != '0') continue;
    if (b1[i] != b2[i]) continue;
    if (b1[i - 1] == '1' && b2[i - 1] == '1' &&
        b1[i + 1] == '0' && b2[i + 1] == '0' &&
        b1[i + 2] == '0' && b2[i + 2] == '0') {
      // Spill-over bucket.
      m_isSpillOver.set(i, true);
    } else if (b1[i - 2] == '0' && b2[i - 2] == '0' &&
               b1[i - 1] == '0' && b2[i - 1] == '0' &&
               b1[i + 1] == '0' && b2[i + 1] == '0' &&
               b1[i + 2] == '0' && b2[i + 2] == '0') {
      // "Empty" bucket.
      m_isEmpty.set(i, true);
    } else if (b1[i - 2] == '0' && b2[i - 2] == '0' &&
               b1[i - 1] == '0' && b2[i - 1] == '0' && 
               b1[i + 1] == '1' && b2[i + 1] == '1') {
      // "Pre-spill" bucket.
      m_isPreSpill.set(i, true);
    }
  }
  info() << nColliding << " colliding bunches." << endmsg;
  m_hFillingScheme->reset();
  for (unsigned int i = 0; i < nBins; ++i) {
    if (b1[i] == '1') m_hFillingScheme->fill(i, 0);
    if (b2[i] == '1') m_hFillingScheme->fill(i, 1);
    if (m_isSpillOver[i]) m_hFillingScheme->fill(i, 2);
    if (m_isPreSpill[i]) m_hFillingScheme->fill(i, 3);
    if (m_isEmpty[i]) m_hFillingScheme->fill(i, 4);
  }
  return true;
}

