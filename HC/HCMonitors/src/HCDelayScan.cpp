// Gaudi
#include "GaudiUtils/HistoLabels.h"
#include "GaudiUtils/Aida2ROOT.h"

// LHCb
// Event/DAQEvent
#include "Event/ODIN.h"
// Event/DigiEvent
#include "Event/HCDigit.h"

// Local
#include "HCDelayScan.h"

using namespace Gaudi::Utils::Histos;

DECLARE_ALGORITHM_FACTORY(HCDelayScan)

//=============================================================================
// Initialisation
//=============================================================================
StatusCode HCDelayScan::initialize() {

  // Initialise the base class.
  StatusCode sc = HCMonitorBase::initialize();
  if (sc.isFailure()) return sc;

  // Setup the histograms.
  const std::vector<std::string> stations = {"B0", "B1", "B2", "F1", "F2"};
  const std::vector<std::string> slots = {"Central", "Previous", "Next"};
  const std::vector<std::string> bxids = {"bb_head", "bb_tail", "ee_head", "ee_tail"};

  const unsigned int nStations = stations.size();
  const unsigned int nSlots = slots.size();
  m_adcs.resize(nStations);
  m_adcsPerStep.resize(nStations);
  for (unsigned int i = 0; i < nStations; ++i) {
    const std::string st = stations[i];
    m_adcs[i].resize(4);
    m_adcsPerStep[i].resize(4);
    for (unsigned int j = 0; j < 4; ++j) {
      const std::string qu = std::to_string(j);
      m_adcs[i][j].resize(nSlots);
      m_adcsPerStep[i][j].resize(nSlots);
      for (unsigned int k = 0; k < nSlots; ++k) {
        const std::string sl = slots[k];
        m_adcsPerStep[i][j][k].resize(4);
        for (unsigned int b = 0; b < 4; ++b) {
          std::string name = "ADC/" + sl + "/" + st + "/" + qu + "/"+ bxids[b];
          m_adcs[i][j][k].push_back(Gaudi::Utils::Aida2ROOT::aida2root(
            bookProfile1D(name, name, m_minStep - 0.5, m_maxStep + 0.5,
                          m_maxStep - m_minStep + 1)));
          for (unsigned int kk = m_minStep; kk < m_maxStep; ++kk) {
            const std::string u = std::to_string(kk);
            name = "ADC/" + u + "/" + st + "/" + qu + "/" + sl + "/" + bxids[b];
            m_adcsPerStep[i][j][k][b].push_back(Gaudi::Utils::Aida2ROOT::aida2root(
              book1D(name, name, -0.5, 1023 + 0.5, 1024)));
          }
        }
      }
    }
  }
  return StatusCode::SUCCESS;
}

//=============================================================================
// Main execution
//=============================================================================
StatusCode HCDelayScan::execute() {

  // Get event information from ODIN.
  const LHCb::ODIN* odin = getIfExists<LHCb::ODIN>(LHCb::ODINLocation::Default);
  if (!odin) {
    return Error("Cannot retrieve ODIN", StatusCode::SUCCESS);
  }
  const unsigned int bxid = odin->bunchId();
  if (bxid < m_bxMin || bxid > m_bxMax) return StatusCode::SUCCESS;

  unsigned int bxidLabel;
  if (bxid == m_bxid_bb_head) {
    bxidLabel = 0;
  } else if (bxid == m_bxid_bb_tail) {
    bxidLabel = 1;
  } else if (bxid == m_bxid_ee_head) {
    bxidLabel = 2;
  } else if (bxid == m_bxid_ee_tail) { 
    bxidLabel = 3;
  } else {
    return Error("Unrecognised BXID " + std::to_string(bxid) + " found.");
  }
  const unsigned int step = odin->calibrationStep();
  if (step < m_minStep || step > m_maxStep) return StatusCode::SUCCESS;

  const std::vector<std::string> locations = {m_digitLocation, 
                                              m_digitLocationPrev, 
                                              m_digitLocationNext};
  for (unsigned int slot = 0; slot < 3; ++slot) {
    const std::string location = locations[slot];
    const LHCb::HCDigits* digits = getIfExists<LHCb::HCDigits>(location);
    if (!digits) {
      return Error("No digits in " + location, StatusCode::FAILURE);
    }

    // Loop over the digits.
    const unsigned int nStations = 5;
    for (unsigned int i = 0; i < nStations; ++i) {
      const bool bwd = i < 3;
      for (unsigned int j = 0; j < 4; ++j) {
        if (m_masked[i][j]) continue;
        LHCb::HCCellID id(m_channels[i][j]);
        const LHCb::HCDigit* digit = digits->object(id);
        if (!digit) {
          const std::string st = bwd ? "B" + std::to_string(i) :
                                       "F" + std::to_string(i - 2);
          warning() << "Cannot retrieve digit for " << st << j << endmsg;
          continue;
        }
        const int adc = digit->adc();
        m_adcs[i][j][slot][bxidLabel]->Fill(step, fadc(adc));
        m_adcsPerStep[i][j][slot][bxidLabel][step - m_minStep]->Fill(fadc(adc));
      }
    }
  }
  return StatusCode::SUCCESS;
}

