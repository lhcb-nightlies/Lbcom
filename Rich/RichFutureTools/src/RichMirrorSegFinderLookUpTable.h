
//-----------------------------------------------------------------------------
/** @file RichMirrorSegFinderLookUpTable.h
 *
 *  Header file for tool : Rich::Future::MirrorSegFinderLookUpTable
 *
 *  @author Chris Jones
 *  @date   2015-02-01
 */
//-----------------------------------------------------------------------------

#pragma once

// STL
#include <sstream>
#include <cmath>
#include <vector>
#include <array>
#include <functional>
#include <limits>

// Gaudi ( must be first ...)
#include "GaudiKernel/ParsersFactory.h"
#include "GaudiKernel/StdArrayAsProperty.h"

// Base class
#include "RichFutureKernel/RichToolBase.h"

// interfaces
#include "RichInterfaces/IRichMirrorSegFinder.h"
#include "RichInterfaces/IRichMirrorSegFinderLookUpTable.h"

// LHCbKernel
#include "Kernel/RichDetectorType.h"
#include "Kernel/RichSide.h"

// RichUtils
#include "RichUtils/BoostArray.h"

// RichDet
#include "RichDet/DeRich1.h"
#include "RichDet/DeRich2.h"

// Vc
#include <Vc/common/alignedbase.h>

namespace Rich
{
  namespace Future
  {

    //-----------------------------------------------------------------------------
    /** @class MirrorSegFinderLookUpTable RichMirrorSegFinderLookUpTable.h
     *
     *  Tool to find the appropriate mirror segment for a given reflection point.
     *  Implementation using a 2D look up table.
     *
     *  @author Chris Jones
     *  @date   2015-02-01
     */
    //-----------------------------------------------------------------------------

    class MirrorSegFinderLookUpTable final : public ToolBase,
                                             public Vc::AlignedBase<Vc::VectorAlignment>,
                                             virtual public IMirrorSegFinderLookUpTable,
                                             virtual public IMirrorSegFinder
    {

    public: // Methods for Gaudi Framework

      /// Standard constructor
      MirrorSegFinderLookUpTable( const std::string& type,
                                  const std::string& name,
                                  const IInterface* parent );

      // Initialization of the tool after creation
      virtual StatusCode initialize() override;

      // Finalization of the tool before deletion
      virtual StatusCode finalize  () override;

    public:

      /// Locates the spherical mirror Segment given a reflection point,
      /// RICH identifier and panel
      const DeRichSphMirror* findSphMirror( const Rich::DetectorType rich,
                                            const Rich::Side side,
                                            const Gaudi::XYZPoint& reflPoint ) const override;

      /// Locates the secondary mirror Segment given a reflection point,
      /// RICH identifier and panel
      const DeRichSphMirror* findSecMirror( const Rich::DetectorType rich,
                                            const Rich::Side side,
                                            const Gaudi::XYZPoint& reflPoint ) const override;

    private: // methods

      /// Update Mirror information on Condition changes
      StatusCode mirrorUpdate();

      /// Load mirrors for the given RICH detector
      void loadMirrors( const Rich::DetectorType rich );
      
      /// Returns the side for a given mirror and Rich Detector
      inline Rich::Side side ( const DeRichSphMirror * mirror, 
                               const Rich::DetectorType rich ) const noexcept
      {
        return ( Rich::Rich1 == rich ? 
                 mirror->mirrorCentre().y() > 0.0 ? Rich::top  : Rich::bottom :
                 mirror->mirrorCentre().x() > 0.0 ? Rich::left : Rich::right  );
      }

      /// Debug printout of the mirrors
      void printMirrors() const;

    private: // data

      /// Flag for the first update
      bool m_firstUpdate{true};

    };

  }
}
