
//-----------------------------------------------------------------------------
/** @file RichTabulatedRefractiveIndex.cpp
 *
 *  Implementation file for class : RichTabulatedRefractiveIndex
 *
 *  @author Chris Jones   Christopher.Rob.Jones@cern.ch
 *  @date 15/03/2002
 */
//-----------------------------------------------------------------------------

// Array properties
#include "GaudiKernel/ParsersFactory.h"
#include "GaudiKernel/StdArrayAsProperty.h"

// local
#include "RichTabulatedRefractiveIndex.h"

using namespace Rich::Future;

DECLARE_TOOL_FACTORY( TabulatedRefractiveIndex )

// Standard constructor
TabulatedRefractiveIndex::
TabulatedRefractiveIndex ( const std::string& type,
                           const std::string& name,
                           const IInterface* parent )
: ToolBase ( type, name, parent )
{
  // Initialise arrays
  m_riches.fill( nullptr );
  m_radiators.fill( nullptr );
  // interface
  declareInterface<IRefractiveIndex>(this);
}

StatusCode TabulatedRefractiveIndex::initialize()
{
  // Initialise base class
  auto sc = ToolBase::initialize();
  if ( sc.isFailure() ) return sc;

  // Get tools
  sc = sc && m_detParams.retrieve();

  // Rich1 and Rich2
  m_riches[Rich::Rich1] = getDet<DeRich1>( DeRichLocations::Rich1 );
  m_riches[Rich::Rich2] = getDet<DeRich2>( DeRichLocations::Rich2 );

  // radiators
  for ( const auto rad : { Rich::Aerogel, Rich::Rich1Gas, Rich::Rich2Gas } )
  {
    if ( m_usedRads[rad] )
    { m_radiators[rad] = getDet<DeRichRadiator>( DeRichLocations::location(rad) ); }
  }

  return sc;
}

double
TabulatedRefractiveIndex::refractiveIndex( const Rich::RadiatorType rad,
                                           const double energy ) const
{
  return deRad(rad)->refractiveIndex(energy,m_hltMode);
}

double
TabulatedRefractiveIndex::refractiveIndex( const Rich::RadiatorType rad,
                                           const double energyBot,
                                           const double energyTop ) const
{
  const auto rich = ( rad == Rich::Rich2Gas ? Rich::Rich2 : Rich::Rich1 );
  return refractiveIndex( rad,
                          m_riches[rich]->nominalPDQuantumEff()->meanX(energyBot,energyTop) /
                          Gaudi::Units::eV );
}

double
TabulatedRefractiveIndex::refractiveIndex( const Rich::RadiatorType rad ) const
{
  return refractiveIndex( rad, m_detParams.get()->meanPhotonEnergy(rad) );
}

double
TabulatedRefractiveIndex::
refractiveIndex ( const Rich::RadIntersection::Vector & intersections,
                  const double energy ) const
{
  // loop over all radiator intersections and calculate the weighted average
  // according to the path length in each radiator
  double refIndex(0), totPathL(0);
  for ( const auto& R : intersections )
  {
    const auto pLength = R.pathLength();
    refIndex += pLength * R.radiator()->refractiveIndex(energy,m_hltMode);
    totPathL += pLength;
  }
  return ( totPathL>0 ? refIndex/totPathL : refIndex );
}

double
TabulatedRefractiveIndex::
refractiveIndex ( const Rich::RadIntersection::Vector & intersections ) const
{
  // loop over all radiator intersections and calculate the weighted average
  // according to the path length in each radiator
  double refIndex(0), totPathL(0);
  for ( const auto& R : intersections )
  {
    const auto energy  = m_detParams.get()->meanPhotonEnergy(R.radiator()->radiatorID());
    const auto pLength = R.pathLength();
    refIndex += pLength * R.radiator()->refractiveIndex(energy,m_hltMode);
    totPathL += pLength;
  }
  return ( totPathL>0 ? refIndex/totPathL : refIndex );
}

double
TabulatedRefractiveIndex::
refractiveIndexRMS ( const Rich::RadIntersection::Vector & intersections ) const
{
  // loop over all radiator intersections and calculate the weighted average
  // according to the path length in each radiator
  double refIndexRMS(0), totPathL(0);
  for ( const auto& R : intersections )
  {
    const auto pLength = R.pathLength();
    const auto * index = R.radiator()->refIndex(m_hltMode);
    refIndexRMS += pLength * index->rms( index->minX(), index->maxX(), 100 );
    totPathL += pLength;
  }
  return ( totPathL>0 ? refIndexRMS/totPathL : refIndexRMS );
}

double
TabulatedRefractiveIndex::
refractiveIndexSD ( const Rich::RadIntersection::Vector & intersections ) const
{
  // loop over all radiator intersections and calculate the weighted average
  // according to the path length in each radiator
  double refIndexSD(0), totPathL(0);
  for ( const auto& R : intersections )
  {
    const auto pLength = R.pathLength();
    const auto * index = R.radiator()->refIndex(m_hltMode);
    refIndexSD += pLength * index->standardDeviation( index->minX(), index->maxX(), 100 );
    totPathL   += pLength;
  }
  return ( totPathL>0 ? refIndexSD/totPathL : refIndexSD );
}
