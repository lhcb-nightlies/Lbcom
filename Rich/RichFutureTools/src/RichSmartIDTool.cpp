
//-----------------------------------------------------------------------------
/** @file RichSmartIDTool.cpp
 *
 * Implementation file for class : Rich::SmartIDTool
 *
 * @author Antonis Papanestis
 * @author Chris Jones   Christopher.Rob.Jones@cern.ch
 * @date 2003-10-28
 */
//-----------------------------------------------------------------------------

// local
#include "RichSmartIDTool.h"

using namespace Rich::Future;

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
SmartIDTool::SmartIDTool( const std::string& type,
                          const std::string& name,
                          const IInterface* parent )
  : ToolBase ( type, name, parent )
{
  // Interface
  declareInterface<ISmartIDTool>(this);
  //declareProperty( "DetectorTool", m_deRichTool );
  //setProperty( "OutputLevel", 1 );
}

//=============================================================================
StatusCode SmartIDTool::initialize()
{
  // Initialise base class
  auto sc = ToolBase::initialize();
  if ( !sc ) return sc;

  sc = sc && m_deRichTool.retrieve();

  // Get the Riches and Rich System
  const auto deRiches = m_deRichTool.get()->deRichDetectors();
  m_richS = deRiches[0]->deRichSystem();

  // loop over riches and photo detector panels
  for ( unsigned int r = 0; r < deRiches.size(); ++r )
  {
    for ( unsigned int panel = 0; panel < Rich::NPDPanelsPerRICH; ++panel )
    {
      (m_photoDetPanels[deRiches[r]->rich()])[panel] = deRiches[r]->pdPanel( (Rich::Side)panel );
      _ri_debug << "Stored photodetector panel "
                << m_photoDetPanels[deRiches[r]->rich()][panel]->name()
                << " offset=" << m_photoDetPanels[deRiches[r]->rich()][panel]->localOffset()
                << endmsg;
    }
  }

  return sc;
}

//=============================================================================
// Returns the position of a RichSmartID cluster in global coordinates
// on the PD entrance window
//=============================================================================
bool
SmartIDTool::globalPosition ( const Rich::PDPixelCluster& cluster,
                              Gaudi::XYZPoint& detectPoint ) const
{
  // Default return status is OK
  bool sc = true;
  if ( 1 == cluster.smartIDs().size() )
  {
    // Handle single pixel clusters differently for speed
    sc = _globalPosition( cluster.smartIDs().front(), detectPoint );
  }
  else
  {
    // reset global coordinate to zero
    detectPoint = Gaudi::XYZPoint(0,0,0);
    // does cluster have any hits !!
    if ( !cluster.smartIDs().empty() )
    {
      // get position for each point in cluster
      for ( const auto S : cluster.smartIDs() )
      {
        Gaudi::XYZPoint tmpP;
        sc = sc && _globalPosition(S,tmpP);
        detectPoint += (Gaudi::XYZVector)tmpP;
      }
      // normalise
      detectPoint /= (double)cluster.size();
    }
  }
  return sc;
}

//=============================================================================
// Returns the position of a RichSmartID in global coordinates
// on the PD entrance window
//=============================================================================
bool
SmartIDTool::globalPosition ( const LHCb::RichSmartID& smartID,
                              Gaudi::XYZPoint& detectPoint ) const
{
  return _globalPosition( smartID, detectPoint );
}

//=============================================================================
// Returns the global position of a local coordinate, in the given RICH panel
//=============================================================================
Gaudi::XYZPoint
SmartIDTool::globalPosition ( const Gaudi::XYZPoint& localPoint,
                              const Rich::DetectorType rich,
                              const Rich::Side side ) const
{
  return panel(rich,side)->PDPanelToGlobalMatrix() * localPoint;
}

//=============================================================================
// Returns the PD position (center of the silicon wafer)
//=============================================================================
bool
SmartIDTool::pdPosition ( const LHCb::RichSmartID& pdid,
                          Gaudi::XYZPoint& pdPoint ) const
{
  // Create temporary RichSmartIDs for two corners of the PD wafer
  LHCb::RichSmartID id1(pdid), id0(pdid);
  id0.setPixelRow( pdid.idType() == LHCb::RichSmartID::MaPMTID ? 1 : 10 );
  id0.setPixelCol( pdid.idType() == LHCb::RichSmartID::MaPMTID ? 1 : 10 );
  id1.setPixelRow( pdid.idType() == LHCb::RichSmartID::MaPMTID ? 6 : 21 );
  id1.setPixelCol( pdid.idType() == LHCb::RichSmartID::MaPMTID ? 6 : 21 );

  // get position of each of these pixels
  Gaudi::XYZPoint a,b;
  const auto sc = _globalPosition(id0,a) && _globalPosition(id1,b);

  // return average position (i.e. PD centre)
  pdPoint = ( sc ?
              Gaudi::XYZPoint( 0.5*(a.x()+b.x()),
                               0.5*(a.y()+b.y()),
                               0.5*(a.z()+b.z()) ) :
              Gaudi::XYZPoint( 0, 0, 0 ) );
  return sc;
}

//=============================================================================
// Returns the SmartID for a given global position
//=============================================================================
bool
SmartIDTool::smartID ( const Gaudi::XYZPoint& globalPoint,
                       LHCb::RichSmartID& smartid ) const
{
  // check to see if the smartID is set, and if PD is active
  if ( smartid.pdIsSet() && !m_richS->pdIsActive(smartid) )
  {
    return false;
  }

  try
  {
    if ( globalPoint.z() < 8000.0 )
    {
      smartid.setRich  ( Rich::Rich1 );
      smartid.setPanel ( globalPoint.y() > 0.0 ? Rich::top : Rich::bottom );
    }
    else
    {
      smartid.setRich  ( Rich::Rich2 );
      smartid.setPanel ( globalPoint.x() > 0.0 ? Rich::left : Rich::right );
    }
    return panel(smartid)->smartID(globalPoint,smartid);
  }

  // Catch any GaudiExceptions thrown
  catch ( const GaudiException & excpt )
  {
    // Print exception as an error
    Error ( "Caught GaudiException " + excpt.tag() + " message '" + excpt.message() + "'" );

    // reset smartid to a default one
    smartid = LHCb::RichSmartID( );

    // return failure status
    return false;
  }

}

//=============================================================================
// Converts a point from the global frame to the detector panel frame
//=============================================================================
Gaudi::XYZPoint
SmartIDTool::globalToPDPanel ( const Gaudi::XYZPoint& globalPoint ) const
{
  return 
    ( globalPoint.z() < 8000.0 ?
      // RICH1
      ( globalPoint.y() > 0.0 ? 
        panel(Rich::Rich1,Rich::top)   ->globalToPDPanelMatrix()*globalPoint :
        panel(Rich::Rich1,Rich::bottom)->globalToPDPanelMatrix()*globalPoint )
      : // RICH2
      ( globalPoint.x() > 0.0 ?
        panel(Rich::Rich2,Rich::left)  ->globalToPDPanelMatrix()*globalPoint :
        panel(Rich::Rich2,Rich::right) ->globalToPDPanelMatrix()*globalPoint ) );
}

//=============================================================================
// Returns a list with all valid smartIDs
//=============================================================================
LHCb::RichSmartID::Vector SmartIDTool::readoutChannelList( ) const
{
  // the list to return
  LHCb::RichSmartID::Vector readoutChannels;

  // Reserve rough size ( RICH1 + RICH2 )
  readoutChannels.reserve( 400000 );

  bool sc = true;

  // Fill for RICH1
  sc = sc && panel(Rich::Rich1,Rich::top)   ->readoutChannelList(readoutChannels);
  sc = sc && panel(Rich::Rich1,Rich::bottom)->readoutChannelList(readoutChannels);
  const auto nRich1 = readoutChannels.size();

  // Fill for RICH2
  sc = sc && panel(Rich::Rich2,Rich::left)  ->readoutChannelList(readoutChannels);
  sc = sc && panel(Rich::Rich2,Rich::right) ->readoutChannelList(readoutChannels);
  const auto nRich2 = readoutChannels.size() - nRich1;

  if ( !sc ) Exception( "Problem reading readout channel lists from DeRichPDPanels" );

  // Sort the list
  SmartIDSorter::sortByRegion(readoutChannels);

  info() << "Created active PD channel list : # channels RICH(1/2) = "
         << nRich1 << " / " << nRich2 << endmsg;

  if ( msgLevel(MSG::VERBOSE) )
  {
    for ( const auto & ID : readoutChannels )
    {
      Gaudi::XYZPoint gPos;
      sc = _globalPosition(ID,gPos);
      if ( !sc ) Exception( "Problem converting RichSmartID to global coordinate" );
      verbose() << " RichSmartID " << ID << " " << ID.dataBitsOnly().key() << endmsg
                << "     -> global Position : " << gPos << endmsg
                << "     -> local Position  : " << globalToPDPanel(gPos) << endmsg;
    }
  }

  return readoutChannels;
}

//=============================================================================

DECLARE_TOOL_FACTORY( SmartIDTool )

//=============================================================================
