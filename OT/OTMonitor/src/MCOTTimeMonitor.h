#ifndef MCOTTIMEMONITOR_H 
#define MCOTTIMEMONITOR_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiHistoAlg.h"
#include "AIDA/IHistogram1D.h"

/** @class MCOTTimeMonitor MCOTTimeMonitor.h
 *  
 *
 *  @author Jan Amoraal
 *  @date   2009-04-22
 */

class MCOTTimeMonitor : public GaudiHistoAlg {

public: 
  /// Standard constructor
  MCOTTimeMonitor( const std::string& name, ISvcLocator* pSvcLocator );

  virtual ~MCOTTimeMonitor( ); ///< Destructor

  StatusCode initialize() override;    ///< Algorithm initialization
  StatusCode execute() override;    ///< Algorithm execution
  StatusCode finalize() override;    ///< Algorithm finalization

protected:

private:
  
  void scale( const HistoID& histoID, const double& weight ) const;
  
private:

  unsigned m_nEvents; ///< Number of events

};
#endif // MCOTTIMEMONITOR_H

void MCOTTimeMonitor::scale( const HistoID& histoID, const double& weight ) const {
  AIDA::IHistogram1D* h =  histo1D( histoID );
  h->scale( weight );
}

