// Event
#include "Kernel/STChannelID.h"

#include "STSelectChannelIDByElement.h"
#include "STDet/DeSTDetector.h"

DECLARE_TOOL_FACTORY( STSelectChannelIDByElement)

STSelectChannelIDByElement::STSelectChannelIDByElement( const std::string& type,
                                    const std::string& name,
                                    const IInterface* parent ) :
  ST::ToolBase(type, name, parent)
{
  declareProperty("elementNames", m_elementNames);
  setForcedInit();
  declareInterface<ISTChannelIDSelector>(this);
}

StatusCode STSelectChannelIDByElement::initialize() {

  StatusCode sc = ST::ToolBase::initialize();
  if (sc.isFailure()) return Error("Failed to initialize", sc);

  for(const auto& name: m_elementNames) {
    DeSTBaseElement* detElement = tracker()->findTopLevelElement(name);
    if (!detElement){
      return Error("Failed to find detector element",StatusCode::FAILURE);
    }
    if( msgLevel(MSG::DEBUG) ) debug() << "Adding " <<  name << " to element list" << endmsg;
    m_detElements.push_back(detElement);
  } // for each

  return StatusCode::SUCCESS;
}

bool STSelectChannelIDByElement::select( const LHCb::STChannelID& id ) const{
  return (*this) (id);
}

bool STSelectChannelIDByElement::operator()( const LHCb::STChannelID& id ) const{
  auto iterElem = std::find_if( m_detElements.begin(), m_detElements.end(),
                                [&](const DeSTBaseElement* elem) { return elem->contains(id); } );
  return iterElem != m_detElements.end()  ;
}
