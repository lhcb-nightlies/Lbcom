// Event
#include "Kernel/STChannelID.h"

#include "STSelectLadders17.h"
#include "STDet/DeSTDetector.h"


DECLARE_TOOL_FACTORY( STSelectLadders17)
 
STSelectLadders17::STSelectLadders17( const std::string& type, 
                                    const std::string& name,
                                    const IInterface* parent ) :
  ST::ToolBase(type, name, parent)
{

  //declareProperty("elementNames", m_elementNames);
  setForcedInit();
  declareInterface<ISTChannelIDSelector>(this);
}


bool STSelectLadders17::select( const LHCb::STChannelID& id ) const{
  return (*this) (id);  
}
  
bool STSelectLadders17::operator()( const LHCb::STChannelID& id ) const{

  unsigned int sector = id.sector();
  return (sector == 1 || sector == 7 ? true:false);
}
