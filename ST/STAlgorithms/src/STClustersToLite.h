#ifndef STCLUSTERSTOLITE_H
#define STCLUSTERSTOLITE_H 1

/** @class STClustersToLite
 *
 *  simple class for stripping liteCluster from clusters
 *  ST specialization
 *
 *  @author M.Needham
 *  @date   02/10/2008
 */

// Gaudi
#include "Event/STCluster.h"
#include "Event/STLiteCluster.h"
#include "Kernel/STAlgBase.h"

#include "SiClustersToLite.h"
#include "SiClusterTraits.h"
typedef SiClustersToLite<LHCb::STCluster> STClustersToLite;
#include "SiClustersToLite.icpp"


template <>
inline SiClustersToLite<LHCb::STCluster>::SiClustersToLite(const std::string& name,
                                                           ISvcLocator* pSvcLocator):
Gaudi::Functional::Transformer<
    typename SiClusterTraits<LHCb::STCluster>::LITECONT(const typename SiClusterTraits<LHCb::STCluster>::CLUSCONT&),
    SiClusterBaseClassTraits<LHCb::STCluster> >(name,
                                                pSvcLocator,
                                                { KeyValue{ "inputLocation", LHCb::STClusterLocation::TTClusters } },
                                                KeyValue{ "outputLocation", LHCb::STLiteClusterLocation::TTClusters } )
{ }

#endif // STClustersToLite
