// LHCbKernel includes
#include "Kernel/STChannelID.h"
#include "STLiteClusterKiller.h"
#include "Kernel/ISTChannelIDSelector.h"

using namespace LHCb;

DECLARE_ALGORITHM_FACTORY( STLiteClusterKiller )

STLiteClusterKiller::STLiteClusterKiller( const std::string& name,
                                    ISvcLocator* pSvcLocator):
  ST::AlgBase(name, pSvcLocator)
{
  declareProperty("SelectorType", m_selectorType = "STRndmChannelIDSelector");
  declareSTConfigProperty("InputLocation",m_inputLocation, STLiteClusterLocation::TTClusters);
  declareSTConfigProperty("SelectorName",m_selectorName , detType() + "LiteKiller");
}

StatusCode STLiteClusterKiller::initialize()
{
  StatusCode sc = ST::AlgBase::initialize();
  if (sc.isFailure()) return Error("Failed to initialize", sc);

  m_clusterSelector = tool<ISTChannelIDSelector>(m_selectorType, m_selectorName);

  return StatusCode::SUCCESS;
}

StatusCode STLiteClusterKiller::execute()
{
  STLiteCluster::STLiteClusters* clusterCont = get<STLiteCluster::STLiteClusters>(m_inputLocation);
  std::remove_if(clusterCont->begin(), clusterCont->end(),
                 [&](const LHCb::STLiteCluster& c)
                 { return (*m_clusterSelector)(c.channelID()); });

  return StatusCode::SUCCESS;
}
