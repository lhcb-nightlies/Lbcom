#ifndef STCLUSTERKILLER_H
#define STCLUSTERKILLER_H 1

#include "Kernel/STAlgBase.h"
#include "Event/STLiteCluster.h"

#include <string>

/** @class STLiteClusterKiller STLiteClusterKiller.h
 *
 *  Class for killing clusters and random
 *
 *  @author M.Needham
 *  @date   06/10/2007
 */

struct ISTChannelIDSelector;

namespace LHCb{
  class STChannelID;
}

class STLiteClusterKiller :public ST::AlgBase {

public:
  
  // Constructor
  STLiteClusterKiller( const std::string& name, ISvcLocator* pSvcLocator); 

  // IAlgorithm members
  StatusCode initialize() override;
  StatusCode execute() override;

private:

  std::string m_selectorName;
  std::string m_selectorType;
  ISTChannelIDSelector* m_clusterSelector = nullptr;

  std::string m_inputLocation;

};

#endif // STCLUSTERKILLER_H
