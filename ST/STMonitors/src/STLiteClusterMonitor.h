#ifndef STLiteClusterMonitor_H
#define STLiteClusterMonitor_H 1

#include "Kernel/STHistoAlgBase.h"

#include "Event/STLiteCluster.h"

/** @class STLiteClusterMonitor STLiteClusterMonitor.h
 *
 *  Class for monitoring STLiteClusters
 *
 *  @author M.Needham
 *  @date   02/12/2008
 */

namespace ST {
  class STLiteClusterMonitor : public HistoAlgBase {

  public:
    
    /// constructor
    STLiteClusterMonitor( const std::string& name, 
                          ISvcLocator *svcloc );

    /// initialize
    StatusCode initialize() override;
    
    /// execute
    StatusCode execute() override;

  private:
    
    typedef LHCb::STLiteCluster::STLiteClusters STLiteClusters;

    void fillHistograms(const LHCb::STLiteCluster& aCluster);
    
    std::string m_clusterLocation;
    
  };
} // End of ST namespace
#endif // STLiteClusterMonitor_H
