################################################################################
# Package: Associators
################################################################################
gaudi_subdir(Associators v3r9p1)

gaudi_depends_on_subdirs(Event/LinkerEvent
                         Event/MCEvent
                         GaudiAlg)

find_package(Boost)
find_package(ROOT)
include_directories(SYSTEM ${Boost_INCLUDE_DIRS} ${ROOT_INCLUDE_DIRS})

gaudi_add_module(Associators
                 src/*.cpp
                 LINK_LIBRARIES LinkerEvent MCEvent GaudiAlgLib)

