#ifndef VeloCCEConfigTool_H
#define VeloCCEConfigTool_H

#include <memory>

// Gaudi
#include "GaudiAlg/GaudiTool.h"
#include "GaudiMath/GaudiMath.h"

// conditons
#include "DetDesc/Condition.h"

// Interface
#include "Kernel/IVeloCCEConfigTool.h"

/** @class VeloCCEConfig VeloCCEConfigTool.h
 *
 *  Tool to determine CCE scan parameters
 *
 *  @author Jon Harrison
 *  @date   2016-03-16
 */

class VeloCCEConfigTool final : public GaudiTool,
                                virtual public IVeloCCEConfigTool {

public:

  /** Constructor */
  VeloCCEConfigTool(const std::string& type,
                    const std::string& name,
                    const IInterface* parent);

  /** Initialize */
  StatusCode initialize() override;

  /** List of killed sensors and bias voltage of test
  * sensor for a step in the CCE scan
  */
  int findKilledSensors(int &CCEstep, std::vector<int> &killSensorList) const override;
  /** List of bad strips for a sensor in the CCE scan
  */
  void findBadStrips(unsigned int &sensorNum, std::vector<int> &badStripList) const override;

private:

  /// set up the tool by reading the conditions
  StatusCode i_loadCondition();

  struct CCEParam final{
    CCEParam() = default;

    /// List of test sensors to kill in tracking
    std::vector< std::vector<int> > sensors;
    /// bias voltage of test sensors
    std::vector<int> voltage;
    /// List of bad strips per sensor for R and phi
    std::vector< std::vector<int> > badStripsR;
    std::vector< std::vector<int> > badStripsPhi;
  };

  CCEParam param;

  /// CCEscanConditions
  Condition* m_CCEscanConditions[2];

};

#endif // VeloCCEConfigTool_H
