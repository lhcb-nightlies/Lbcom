#include <sstream>

#include <time.h>

#include "PvssTell1Names.h"

//-----------------------------------------------------------------------------
// Implementation file for class : PvssTell1Names
//
// 2008-09-08 : Kurt Rinnert
//-----------------------------------------------------------------------------

namespace Velo
{
  
  DECLARE_TOOL_FACTORY( PvssTell1Names )
  
  //=============================================================================
  // Standard constructor
  //=============================================================================
  PvssTell1Names::PvssTell1Names( const std::string& type,
      const std::string& name,
      const IInterface* parent )
    : GaudiTool ( type, name , parent )
    {
      declareInterface<Velo::IPvssTell1Names>(this);
    }

  //=============================================================================
  // Access to PVSS TELL1 name by sensor number
  //=============================================================================
  const std::string& PvssTell1Names::pvssName( unsigned int sensorNumber )
  {
    return m_pvssNames.pvssName(sensorNumber);
  }

}
